# M102 - Fruit Detector

This repository contains codes for Team M102's Arduino-based fruit detector robot.

## Content deatils

The main block of code running on arduino is in arduino\arduino_main\ folder.
arduino_main.ino is the main sketch file uploaded into arduino;
interface.h and interface.cpp contains variety of functions that helps arduino communicate with the sensors;
Navigator.h and Navigator.cpp implements a class Navigator that is responsible for basic naviagtion decision algorithms (although not really used in the actual run)

For other folders:

arduino\arduino_test\ folder contains sketches and functions used in the earlier development stage to check whether the circuits and the sensors are working correctly. It is frequently changed and may not be a fully running program.

python\ folder includes two python files: establish_bt_connection.py is a test code written to connect python with arduino via bluetooth. This worked fine, but was not used in the actual run. main.py was planned to be used as a main python program that will be run alongside arduino, but was not developed.

.vscode\ folder contains configuration data used for the VS Code IDE to correctly compile, verify and upload arduino/C++ codes. This may not work in other workstations.

## Notes on algorithms not implemented in this repository

- The blinkNormal() method was not fully implemented and didn't run as planned in the actual run.
- The navigation algorithm for the return past the tunnel was not fully implemented.
- The algorithm for the robot to refrain from touching the unripe fruit was not implemented.

## Major changes made in the code

- Python files were never used in the competition. Bluetooth functionality also.
- The Navigator.sweep() and Navigator.moveUntilAligned() methods were not used in actual run as another simpler algorithm was introduced and was written in the arduino_main.ino sketch file directly.


