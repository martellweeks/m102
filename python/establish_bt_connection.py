import serial
import time

BTSerial = serial.Serial(port = 'com8', baudrate = 115200)

time.sleep(2)

while(1):
    print("In cue: ", BTSerial.in_waiting)
    data = ""
    while(BTSerial.in_waiting > 0) :
        data = (BTSerial.readline()).decode()
        print(data)
    val = input()
    BTSerial.write(str.encode(val))
    time.sleep(0.3)