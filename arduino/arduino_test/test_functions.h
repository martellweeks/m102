#ifndef test_functions.h
#define test_functions.h

#include "Arduino.h"
#include <Adafruit_MotorShield.h>
#include <SharpIR.h>


void test_Led(int GLed, int ALed, int RLed) {
  Serial.print("Test for LED");
  // Set pinmode to output
  pinMode(GLed, OUTPUT);
  pinMode(ALed, OUTPUT);
  pinMode(RLed, OUTPUT);
  // Turns each LED on then off with 1s delay
  digitalWrite(GLed, HIGH);
  delay(1000);
  digitalWrite(GLed, LOW);
  delay(1000);
  digitalWrite(ALed, HIGH);
  delay(1000);
  digitalWrite(ALed, LOW);
  delay(1000);
  digitalWrite(RLed, HIGH);
  delay(1000);
  digitalWrite(RLed, LOW);
  delay(1000);
}

void test_LineSensor(int LineSensor) {
  Serial.print("Test for Line Sensor");
  // Set pin to input
  pinMode(LineSensor, INPUT);
  //If on line, Green LED comes on; if off line, Red LED comes on; Repeats five times
  for(int i = 1; i < 6; i++) {
    Serial.print("Test ");
    Serial.print(i);
    Serial.println(" of 5");
    int LineState = digitalRead(LineSensor);
    if (LineState == HIGH) {
      Serial.println("Line ON");
    } else {
      Serial.println("Line OFF");
    }
    delay(1000);
  }
}

void test_LDRAnalog(int LDR) {
  Serial.print("Test for LDR, Analog pin");
  pinMode(LDR, INPUT); //Set pinmode to input
  //Reads off LDR data value; Repeats five times
  for(int i = 1; i < 6; i++) {
    Serial.print("Test ");
    Serial.print(i);
    Serial.println(" of 5");
    int LDRData = analogRead(LDR);
    Serial.println(LDRData);
        delay(2000);
  }
}

void test_LDRDigital(int LDR) {
  Serial.println("Test for LDR, Digital pin");
  pinMode(LDR, INPUT); //Set pinmode to input
  //Reads off LDR data value; Repeats five times
  for(int i = 1; i < 6; i++) {
    Serial.print("Test ");
    Serial.print(i);
    Serial.println(" of 5");
    int LDRData = digitalRead(LDR);
    Serial.println(LDRData);
    delay(2000);
  }
}

void test_DistanceSensor(SharpIR DistanceSensor) {
  Serial.println("Test for distance sensor");
  for(int i = 1; i < 6; i++) { 
    Serial.print("Test ");
    Serial.print(i);
    Serial.println(" of 5");
    int dist_cm = DistanceSensor.distance();
    Serial.println(dist_cm);
    delay(2000);
  }
}
void test_MotorWheels(Adafruit_DCMotor* LeftMotor, Adafruit_DCMotor* RightMotor) {
  Serial.print("Test for motors");
  //Runs motors in medium speed, then speed up left to max, change direction of right, then release
  LeftMotor->setSpeed(100);
  RightMotor->setSpeed(100);
  LeftMotor->run(FORWARD);
  RightMotor->run(FORWARD);
  delay(4000);
  LeftMotor->setSpeed(255);
  RightMotor->run(BACKWARD);
  delay(4000);
  LeftMotor->run(RELEASE);
  RightMotor->run(RELEASE);
}

#endif
