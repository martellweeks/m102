#include <Adafruit_MotorShield.h>
#include <Wire.h>
#include <SharpIR.h>
#include "test_functions.h"

// Declare motors
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *LeftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *RightMotor = AFMS.getMotor(2);
Adafruit_DCMotor *ChassisMotor = AFMS.getMotor(3);

// Assort pin numbers to correct components and declare distance sensor class
const int g_LDR = 6, g_LineSensor = 7, g_GLed = 8, g_ALed = 9, g_RLed = 10; //Digital ports
const int g_DistSensor = A0; //Analog ports
SharpIR DistanceSensor = SharpIR(g_DistSensor, 1080);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println("Serial connected");
  AFMS.begin();
}

void loop() {
  // put your main code here, to run repeatedly:

  // Run series of test functions 
  // test_LDRDigital(g_LDR);
  // test_LineSensor(g_LineSensor);
  // test_DistanceSensor(DistanceSensor);
  test_MotorWheels(LeftMotor, RightMotor);
  
}
