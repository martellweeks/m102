
#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include "Navigator.h"
#include "interface.h"
#include <Chrono.h>

using namespace std;

int * Navigator::getSensors() {
    // Reads off all 4 line sensors and returns an int array listing the sensor values
    static int values[4];
    values[0] = digitalRead(Navigator::sensor1);
    values[1] = digitalRead(Navigator::sensor2);
    values[2] = digitalRead(Navigator::sensor3);
    values[3] = digitalRead(Navigator::sensor4);
    return values;
}

char Navigator::sweep() {
    // Actually we did not use this code!
    // Sweeps both left and right to find the right pathway, and returns char corresponding to L or R
    bool left = false, right = false;
    char val;
    Chrono localChrono;
    // Sweep to right
    rotateMotor(Navigator::LeftMotor, 100, FORWARD);
    rotateMotor(Navigator::RightMotor, 0, RELEASE);
    while(localChrono.elapsed() < 1000) {
        if(Navigator::getSensors()[0] == 1) {
            right = true;
        }
    }

    rotateMotor(Navigator::LeftMotor, 100, BACKWARD);
    delay(1000);

    // Sweep to left
    rotateMotor(Navigator::LeftMotor, 0, RELEASE);
    rotateMotor(Navigator::RightMotor, 100, FORWARD);
    localChrono.restart();
    while(localChrono.elapsed() < 1000) {
        if(Navigator::getSensors()[0] == 1) {
            left = true;
        }
    }

    rotateMotor(Navigator::RightMotor, 100, BACKWARD);
    delay(1000);
    rotateMotor(Navigator::RightMotor, 0, RELEASE);
    
    if(left && right) {
        val = 'C'; // Centre
    } else if (left && !right) {
        val = 'L'; // Move left
    } else if (!left && right) {
        val = 'R'; // Move right
    } else {
        val = '0'; // None
    }

    if(val == '0' || val == 'C') {
        // Check for T junction
        rotateMotor(Navigator::LeftMotor, 150, FORWARD);
        rotateMotor(Navigator::RightMotor, 150, FORWARD);
        localChrono.restart();
        int caseT[4] = {0, 1, 1, 1};
        if(Navigator::getSensors() == caseT) {
            val = 'T'; // T junction
        } else if(localChrono.elapsed() > 2000) {
            val = 'S'; // Straight line, follow on OR something's really wrong
        }
        rotateMotor(Navigator::LeftMotor, 0, RELEASE);
        rotateMotor(Navigator::RightMotor, 0, RELEASE);
    }

    return val;
}

void Navigator::moveUntilAligned(char direction) {
    // We didn't use this code either!
    // Receives char from sweep, and moves slowly until the robot is back on line
    if(direction == 'L') {
        while(Navigator::getSensors()[0] != 1 && Navigator::getSensors()[2] != 1) {
            rotateMotor(Navigator::LeftMotor, 50, BACKWARD);
            rotateMotor(Navigator::RightMotor, 150, FORWARD);
        }
    } else if(direction == 'R') {
        while(Navigator::getSensors()[0] != 1 && Navigator::getSensors()[2] != 1) {
            rotateMotor(Navigator::LeftMotor, 150, FORWARD);
            rotateMotor(Navigator::RightMotor, 50, BACKWARD);
        }
    } else if(direction == 'T') {
        while(Navigator::getSensors()[0] != 1 && Navigator::getSensors()[2] != 1) {
            rotateMotor(Navigator::LeftMotor, 100, FORWARD);
            rotateMotor(Navigator::RightMotor, 100, BACKWARD);
        }
    }
    delay(400); // Allow the frontal sensor to get into centre of line
    rotateMotor(Navigator::LeftMotor, 0, RELEASE);
    rotateMotor(Navigator::RightMotor, 0, RELEASE);
}
