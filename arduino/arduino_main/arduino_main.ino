#include <SharpIR.h>
#include <Adafruit_MotorShield.h>
#include "interface.h"
#include "Navigator.h"
#include <Chrono.h>

// Declare motors
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *LeftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *RightMotor = AFMS.getMotor(2);
Adafruit_DCMotor *ChassisMotor = AFMS.getMotor(3);

// Assort pin numbers and declare classes
const int g_LDR = 6, g_ALed = 11, g_GLed = 12, g_RLed = 13; // Digital ports
const int g_LineSensorA = 7, g_LineSensorB = 8, g_LineSensorC = 9, g_LineSensorD = 10; // Digital line sensor ports
const int g_DistanceSensor = A0; //Analog ports
SharpIR DistanceSensor = SharpIR(g_DistanceSensor, 1080); // Distance sensor class, returns distance to cm
Navigator Nav = Navigator(g_LineSensorA, g_LineSensorB, g_LineSensorC, g_LineSensorD, LeftMotor, RightMotor); // Navigator class

// Declare relevant variables
int idealState[] = {1, 0, 1, 0}; // Ideal state of following line
int offLine[] = {0, 0, 1, 0}; // State of slightly off line;
int firstJunction[] = {1, 1, 1, 0}; // State of entering the first junction
int TJunction[] = {0, 1, 1, 1}; // State of being at the end of T junction
int outJunction[] = {1, 0, 1, 1}; // State of returning to junction, whether T or first one
int turnLeft[] = {0, 1, 0, 0}; 
int turnRight[] = {0, 0, 0, 1};
// Actually I'm not using any of the arrays above
bool doesItBlink = false; // Whether the amber light is flashing while it's moving
bool pastT = false; // Whether the robot passed the T junction
bool pastT2 = false; // Whether the robot passed T junction twice (out of the loop)

void setup() {
    // Bluetooth serial setup
    // pinMode(NINA_RESETN, OUTPUT);         
    // digitalWrite(NINA_RESETN, LOW);
    
    // SerialNina.begin(115200);
    // SerialNina.write("BT Serial Opened"); // BT setup done
    Serial.begin(115200);
    Serial.println("Serial opened");

    // Set pin states to either input or output
    pinMode(g_LDR, INPUT);
    pinMode(g_ALed, OUTPUT);
    pinMode(g_GLed, OUTPUT);
    pinMode(g_RLed, OUTPUT);
    pinMode(g_LineSensorA, INPUT);
    pinMode(g_LineSensorB, INPUT);
    pinMode(g_LineSensorC, INPUT);
    pinMode(g_LineSensorD, INPUT);
    pinMode(g_DistanceSensor, INPUT);

    AFMS.begin();
}

void loop() {

    // Initiate motor movement and start blinking the amber LED
    if (doesItBlink == false) {
       blinkNormal(g_ALed);
       doesItBlink = true;
    }
    blinkNormal(g_ALed); // Blink!
    // Initial movement
    rotateMotor(LeftMotor, 150, FORWARD);
    rotateMotor(RightMotor, 150, FORWARD);
    
    // Code for line navigation algorithm
    int * lineStatus = Nav.getSensors();
    
    if(lineStatus[0] == 0 && lineStatus[1] == 0 && lineStatus[2] == 0 && lineStatus[3] == 1) {
      // Turn right condition
      rotateMotor(LeftMotor, 0, RELEASE);
      rotateMotor(RightMotor, 0, RELEASE);
      delay(1000);
      int * lineStatus2 = Nav.getSensors();
      while(lineStatus2[2] != 1 || lineStatus2[3] != 0) {
        lineStatus2 = Nav.getSensors();
        rotateMotor(LeftMotor, 140, FORWARD);
        rotateMotor(RightMotor, 40, BACKWARD);
      }
    }

    if(lineStatus[0] == 0 && lineStatus[1] == 1 && lineStatus[2] == 0 && lineStatus[3] == 0) {
      // Turn left condition
      rotateMotor(LeftMotor, 0, RELEASE);
      rotateMotor(RightMotor, 0, RELEASE);
      delay(1000);
      int * lineStatus2 = Nav.getSensors();
      while(lineStatus2[2] != 1 || lineStatus2[1] != 0) {
        lineStatus2 = Nav.getSensors();
        rotateMotor(LeftMotor, 40, BACKWARD);
        rotateMotor(RightMotor, 140, FORWARD);
      }
    }
    
    if(lineStatus[0] == 0 && lineStatus[1] == 1 && lineStatus[2] == 1 && lineStatus[3] == 1) {
      // Entering T
      rotateMotor(LeftMotor, 0, RELEASE);
      rotateMotor(RightMotor, 0, RELEASE);
      delay(1000);
      int * lineStatus2 = Nav.getSensors();
      while(lineStatus2[0] != 1) {
        lineStatus2 = Nav.getSensors();
        rotateMotor(LeftMotor, 160, FORWARD);
        rotateMotor(RightMotor, 140, BACKWARD);
      }
      while(lineStatus2[2] != 1) {
        lineStatus2 = Nav.getSensors();
        rotateMotor(LeftMotor, 60, FORWARD);
        rotateMotor(RightMotor, 60, FORWARD);
      }
      pastT = true; // Alert that it past the T junction
    }
    
    if(lineStatus[0] == 1 && lineStatus[1] == 0 && lineStatus[2] == 1 && lineStatus[3] == 1 && pastT) {
      // Condition for getting out of loop
      rotateMotor(LeftMotor, 0, RELEASE);
      rotateMotor(RightMotor, 0, RELEASE);
      delay(1000);
      int * lineStatus2 = Nav.getSensors();
      while(lineStatus2[0] != 1 || lineStatus2[2] != 1) {
        lineStatus2 = Nav.getSensors();
        rotateMotor(LeftMotor, 140, FORWARD);
        rotateMotor(RightMotor, 140, BACKWARD);
      }
      rotateMotor(LeftMotor, 60, FORWARD);
      rotateMotor(RightMotor, 60, FORWARD);
      delay(1000);
      pastT = false; 
      pastT2 = true; // Alert it's out of T junction
    }
    
    if(lineStatus[0] == 0 && lineStatus[1] == 0 && lineStatus[2] == 1 && lineStatus[3] == 1 && pastT2) {
      // Condition for getting into the final line
      rotateMotor(LeftMotor, 0, RELEASE);
      rotateMotor(RightMotor, 0, RELEASE);
      delay(1000);
      int * lineStatus2 = Nav.getSensors();
      while(lineStatus[1] != 0 || lineStatus2[2] != 1 || lineStatus2[3] != 0) {
        lineStatus2 = Nav.getSensors();
        rotateMotor(LeftMotor, 40, BACKWARD);
        rotateMotor(RightMotor, 140, FORWARD);
      }
      pastT2 = false;
    }
    
    if(pastT) {
      //If block for when it's in the fruit loop - includes sensing fruits
      int distance = readDistSensor(DistanceSensor); // Now read distance
      if(distance <= 10) { // When near the fruit
          // doesItBlink = false;
          rotateMotor(LeftMotor, 255, FORWARD);
          rotateMotor(RightMotor, 255, FORWARD);
          delay(235); // 235ms to move 5cm, when full speed of 40rpm
          rotateMotor(LeftMotor, 0, RELEASE);
          rotateMotor(RightMotor, 0, RELEASE);
          blinkLED(g_ALed, 1000); //Constant amber LED for 1 second
          // doesItBlink = true;
          rotateMotor(LeftMotor, 255, FORWARD);
          rotateMotor(RightMotor, 255, FORWARD);
          delay(188); // 188ms to move45cm, when full speed of 40rpm, so stops 1cm from fruit
          rotateMotor(LeftMotor, 0, RELEASE);
          rotateMotor(RightMotor, 0, RELEASE);
          int colour = readLDR(g_LDR); // Use colour detector to detect colour
          if(colour == 0) { // Unripe fruit - go straight
              blinkLED(g_GLed, 5000);
              rotateMotor(LeftMotor, 255, FORWARD);
              rotateMotor(RightMotor, 255, FORWARD);
              delay(400);
          } else { // Ripe fruit - open chassis and go straight then close chassis again
              //Open chassis - to be developed
              blinkLED(g_RLed, 5000);
              rotateMotor(LeftMotor, 255, FORWARD);
              rotateMotor(RightMotor, 255, FORWARD);
              delay(400);
              // Close chassis - to be developed
          }
        }
    }

    
    


    
    
