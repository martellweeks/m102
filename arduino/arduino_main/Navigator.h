#ifndef navigator.h
#define navigator.h

#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include "interface.h"
#include <Chrono.h>

class Navigator // Class that takes all 4 line sensors and two motor wheels as components and makes basic navigation decisions
{
  private:
      int sensor1, sensor2, sensor3, sensor4;
      Adafruit_DCMotor *LeftMotor, *RightMotor;
  public:
      Navigator(int _sensor1, int _sensor2, int _sensor3, int _sensor4, Adafruit_DCMotor* _LeftMotor, Adafruit_DCMotor* _RightMotor) {
          sensor1 = _sensor1;
          sensor2 = _sensor2;
          sensor3 = _sensor3;
          sensor4 = _sensor4;
          LeftMotor = _LeftMotor;
          RightMotor = _RightMotor;
      }

      int * getSensors(); // Reads off all 4 line sensors and returns an int array listing the sensor values
      char sweep(); // Sweeps both left and right to find the right pathway, and returns char corresponding to L or R
      void moveUntilAligned(char direction); // Receives char from sweep, and moves slowly until the robot is back on line
};

#endif
