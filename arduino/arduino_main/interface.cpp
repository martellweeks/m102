#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include "interface.h"
#include <SharpIR.h>

void blinkLED(int pin, int time) {
    // Blinks LED in a given time variable; leaves LED turned on when time = -1 
    pinMode(pin, OUTPUT);
    digitalWrite(pin, HIGH);
    if (time != -1) {
        delay(time);
        digitalWrite(pin, LOW);
    }
}

void blinkNormal(int pin) {
    // Blinks LED every 0.5 seconds by using millis
    // Will be surprised if this actually works well
    pinMode(pin, OUTPUT);
    int ledState = LOW;
    unsigned long currentMillis = millis();
    long previousMillis = 0;
    long interval = 500;
    if(currentMillis - previousMillis > interval) {
      previousMillis = currentMillis;
      if(ledState == LOW) {
        ledState = HIGH;
      }
      else {
        ledState = LOW;
      }
    digitalWrite(pin, ledState);
    }
}

void offLED(int pin) {
    pinMode(pin, OUTPUT);
    digitalWrite(pin, LOW);
}

int readLineSensor(int pin) {
    // Reads in digital value of line sensor and returns the value
    pinMode(pin, INPUT);
    int val = digitalRead(pin);
    return val;
}

int readDistSensor(SharpIR distSensor) {
    // Reads in analog value of distance sensor and returns the value
    int dist_cm = distSensor.distance();
    return dist_cm;
}

int readLDR(int pin) {
    // Reads in analog value of LDR which detects reflected red LED light from fruit, and returns the value
    pinMode(pin, INPUT);
    int val = digitalRead(pin);
    return val;
}

void rotateMotor(Adafruit_DCMotor *Motor, int speed, int direction) {
    // Rotates a motor with given speed and direction
    Motor->setSpeed(speed);
    Motor->run(direction);
}
