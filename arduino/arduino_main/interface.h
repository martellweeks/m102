#ifndef interface.h
#define interface.h

#include <Arduino.h>
#include <Adafruit_MotorShield.h>
#include <SharpIR.h>

void blinkLED(int pin, int time);

void blinkNormal(int pin);

void offLED(int pin);

int readLineSensor(int pin);

int readDistSensor(SharpIR distSensor);

int readLDR(int pin);

void rotateMotor(Adafruit_DCMotor *Motor, int speed, int direction);

#endif interface.h