#include "Arduino.h"
#include "Adafruit_MotorShield.h"
#include "interface.h"

void blinkLED(int pin, int time) {
    // Blinks LED in a given time variable; leaves LED turned on when time = -1 
    pinMode(pin, OUTPUT);
    digitalWrite(pin, HIGH);
    if (time != -1) {
        delay(time);
        digitalWrite(pin, LOW);
    }
}

int readLineSensor(int pin) {
    // Reads in digital value of line sensor and returns the value
    pinMode(pin, INPUT);
    int val = digitalRead(pin);
    return val;
}

int readDistSensor(int pin) {
    // Reads in analog value of distance sensor and returns the value
    pinMode(pin, INPUT);
    int val = analogRead(pin);
    return val;
}

int readColorLDR(int pin) {
    // Reads in analog value of LDR which detects reflected red LED light from fruit, and returns the value
    pinMode(pin, INPUT);
    int val = analogRead(pin);
    return val;
}

void rotateMotor(Adafruit_DCMotor* Motor, int speed, int direction) {
    // Rotates a motor with given speed and direction
    Motor->setSpeed(speed);
    Motor->run(direction);
}
