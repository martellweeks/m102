#ifndef interface.h
#define interface.h

#include <Adafruit_MotorShield.h>

void blinkLED(int pin, int time);

int readLineSensor(int pin);

int readDistSensor(int pin);

int readColorLDR(int pin);

void rotateMotor(Adafruit_DCMotor Motor, int direction);

#endif interface.h